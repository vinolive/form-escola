const express = require('express');
const fs = require('fs');
const app = express();

var porta = 3000
app.listen(3000);

app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
})
app.get('/todosalunos', (req, res) => {
    res.sendFile(__dirname + '/cadalunos.html');
})
app.post('/salvar', (req, res) => {
    var sexo = req.body.nmSexo;
    var alunoString =`Nome: ${req.body.nmNome} ${req.body.nmSobreNome}\nNascimento: ${req.body.nmDataNasc}\nEmail: ${req.body.nmEmail}\nEndereco: ${req.body.nmEndereco}\nSerie: ${req.body.nmSerie}º\nSexo: ${sexo};\n`

    fs.appendFile("alunos.txt", alunoString, function(err) {
        if (err) {
            throw err
        } else {
            console.log("Salvou o aluno: " + alunoString);  
        }
    })

    res.sendFile(__dirname + "/cadalunos.html")
})

      